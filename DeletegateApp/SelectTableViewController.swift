//
//  SelectTableViewController.swift
//  DeletegateApp
//
//  Created by Abdul Rehman on 08/09/2019.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit


protocol selectMoviedelegate: class {
    func movieSelected(selectedMovie:String)
}

class SelectTableViewController: UITableViewController {
    
    let movieTitiles = ["Matrix" , "Avengers End Game" , "Captain America" , "Thor" , "Inception"]
    var selectedIndex : NSInteger = 0
    weak var delegateVar: selectMoviedelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    
    }

    @IBAction func done() {
        dismiss(animated: true, completion: nil)
        delegateVar?.movieSelected(selectedMovie: movieTitiles[selectedIndex])
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return movieTitiles.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scificmovie", for: indexPath)
        
        cell.textLabel?.text = movieTitiles[indexPath.row]
        
        if indexPath.row == selectedIndex {
            cell.accessoryType = .checkmark
        }
        else {
            cell.accessoryType = .none
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        selectedIndex = indexPath.row
        tableView.reloadData()
        tableView.deselectRow(at: indexPath, animated: true)
    }
    

}
