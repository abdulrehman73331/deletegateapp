//
//  ViewController.swift
//  DeletegateApp
//
//  Created by Abdul Rehman on 08/09/2019.
//  Copyright © 2019 Abdul Rehman. All rights reserved.
//

import UIKit


class ViewController: UIViewController , selectMoviedelegate {
    func movieSelected(selectedMovie: String) {
        movieNameBTN.setTitle(selectedMovie, for: .normal)
    }
    
    @IBOutlet weak var movieNameBTN: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "scific" {
            let navigationController = segue.destination as! UINavigationController
            
            let destinationController = navigationController.topViewController as! SelectTableViewController
            
            destinationController.delegateVar = self
            
        }
    
    }
    

}

